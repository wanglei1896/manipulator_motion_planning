function [x_result,f_result,cost_time] = trajectory_planningf_PSO(para,sizepop,maxgen,fitnessf,interplot,sumplot)
%TRAJECTORY_PLANNING3F_PSO 此处显示有关此函数的摘要
%   此处显示详细说明
global bound
if nargin==5
    sumplot=interplot;
end
%% 参数初始化
c1 = 1.49445;
c2 = 1.49445;

numvar=size(bound,1);

fbest_store=zeros(1,maxgen);
bsol_store=zeros(maxgen,numvar);

Vmax = 1; 
Vmin = -1;

%% 产生初始粒子和速度
pop = (ones(sizepop,1)*(bound(:,2)-bound(:,1))').*(rand(sizepop,numvar))+...
    (ones(sizepop,1)*bound(:,1)');    %初始种群
V = rands(sizepop,numvar);  %初始化速度
% 计算适应度
fitness = fitnessf(pop, para);

%% 个体极值和群体极值
[bestfitness bestindex] = max(fitness);
zbest = pop(bestindex,:);   %全局最佳
gbest = pop;    %个体最佳
fitnessgbest = fitness;   %个体最佳适应度值
fitnesszbest = bestfitness;   %全局最佳适应度值

%% 迭代寻优
tic
i=1
% figure
while i<5 || (abs(fbest_store(i-1)-fbest_store(i-2))+abs(fbest_store(i-2)-fbest_store(i-3))...
        +abs(fbest_store(i-3)-fbest_store(i-4)))/(3*abs(fbest_store(i-1)))>0.005
    for j = 1:sizepop
        % 速度更新
        V(j,:) = V(j,:) + c1*rand*(gbest(j,:) - pop(j,:)) + c2*rand*(zbest - pop(j,:));
        V(j,find(V(j,:)>Vmax)) = Vmax;
        V(j,find(V(j,:)<Vmin)) = Vmin;
        % 种群更新
        pop(j,:) = pop(j,:) + V(j,:);
        max_i=find(pop(j,:)>bound(:,2)');
        pop(j,max_i) = bound(max_i,2)';
        min_i=find(pop(j,:)<bound(:,1)');
        pop(j,min_i) = bound(min_i,1)';
    end
    % 适应度值更新
    fitness = feval(fitnessf, pop, para);
    for j = 1:sizepop
        % 个体最优更新
        if fitness(j) > fitnessgbest(j)
            gbest(j,:) = pop(j,:);
            fitnessgbest(j) = fitness(j);
        end
        % 群体最优更新
        if fitness(j) > fitnesszbest
            zbest = pop(j,:);
            fitnesszbest = fitness(j);
        end
    end 
    %%%%%%%%%%%
%     fbest_store(i)=fitnesszbest;
    [fbest_store(i),bind]=max(fitness);
    bsol=pop(bind,:);
    bsol_store(i,:)=bsol;
    %%%%%%%%%%%
    %*************************plot results
    if nargin==6
        feval(interplot,para,bsol,1/fbest_store(i),i,maxgen);
        pause(0)
    end
    i=i+1;
end
toc
if nargin>4
    sumplot(maxgen,fbest_store,bsol_store);
end

x_result=zbest;
f_result=1/fitnesszbest;
cost_time=toc;
end