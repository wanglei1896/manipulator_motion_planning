function [message,joint_co]=invkin3(cartesian_co,phi,link)
% l1=1;l2=1;l3=0.5;
x=cartesian_co(1);
y=cartesian_co(2);
l1=link(1); l2=link(2); l3=link(3);
xx=x-l3*cos(phi);
yy=y-l3*sin(phi);
k=(xx^2+yy^2-l1^2-l2^2)/(2*l1*l2);
t2=acos(k);

if (x^2+y^2)==(l1+l2+l3)^2
    message='singular';
    joint_co=[atan2(y,x) 0 0];
    return;
end
while k > 1
%     phi=-pi+rand*2*pi;
%     xx=x-l3*cos(phi);
%     yy=y-l3*sin(phi);
%     k=(xx^2+yy^2-l1^2-l2^2)/(2*l1*l2);
%     t2=-acos(k);
    message='no-redundancy';
    joint_co=[k 0 0];
    return;
end
d=l1^2+l2^2+2*l1*l2*cos(t2);
cc=(xx*(l1+l2*cos(t2))+yy*l2*sin(t2))/d;
ss=(-xx*l2*sin(t2)+yy*(l1+l2*cos(t2)))/d;
t1=atan2(ss,cc);
if d==0
    t1=phi-pi/2;
end
t3=phi-t1-t2;
message='ok';
joint_co=[ t1 t2 t3];
end