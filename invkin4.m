function [b,a]=invkin4(x,y,phi1,phi2,l)
l1=l(1); l2=l(2); l3=l(3); l4=l(4);
x1=x-l4*cos(phi1);
y1=y-l4*sin(phi1);
x2=x1-l3*cos(phi2);
y2=y1-l3*sin(phi2);
k=(x2^2+y2^2-l1^2-l2^2)/(2*l1*l2);
t2=acos(k);
while k > 1
%     phi1=-pi+rand*2*pi;
%     phi2=-pi+rand*2*pi;
%     x1=x-l4*cos(phi1);
%     y1=y-l4*sin(phi1);
%     x2=x1-l3*cos(phi2);
%     y2=y1-l3*sin(phi2);
%     k=(x2^2+y2^2-l1^2-l2^2)/(2*l1*l2);
%     t2=-acos(k);
    b='no-redundancy';
    a=[k 0 0 0];
    return;
end
d=l1^2+l2^2+2*l1*l2*cos(t2);
cc=(x2*(l1+l2*cos(t2))+y2*l2*sin(t2))/d;
ss=(-x2*l2*sin(t2)+y2*(l1+l2*cos(t2)))/d;
t1=atan2(ss,cc);
t3=phi2-t1-t2;
t4=phi1-phi2;
b='ok';
a=[t1 t2 t3 t4];
end

