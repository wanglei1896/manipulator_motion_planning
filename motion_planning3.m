global bound pointnum internum x_dimension l f1 f2 f3 f4 result ft_s para
l=[1,1,1];
f1=figure;
f2=figure;
% f3=figure;
f4=figure;
pointnum=20;    %描述曲线的输入点的个数
internum=40;   %插值点个数，也即最后输出的描述机械臂画出的点的个数
x_dimension=5;  %待优化参数的维度
bound=[-ones(x_dimension,1),ones(x_dimension,1)];
bound(x_dimension+1,:)=[0.01,10];  %最后一个待优化参数是机械臂运动的真实时间(s)

[px,py] = forword_traj(pointnum);
% [px,py,pointnum] = polygon_traj(5,2,10);
%计算phi的范围，插值
theta=zeros(1,pointnum);
for i=1:pointnum
    k=(px(i)^2+py(i)^2+l(3)^2-(l(1)+l(2))^2)/(2*sqrt(px(i)^2+py(i)^2)*l(3));
   	if k<=-1
     	theta(i)=pi;
    else
       	theta(i)=acos(k);
  	end
end

para=[px;py;theta];
% figure(f2)
% x=linspace(0,2*pi,pointnum);
% t=atan2(py,px);
% intervern=[t-theta;t+theta];
% plot(x,intervern,'.','color','r','MarkerSize',20);hold on;
% area(x,intervern(1,:),'FaceColor',[0 0.75 0.75]);hold on;
% area(x,intervern(2,:),'FaceColor',[0 0.75 0.75]);hold on;

% cacu_result(3,para,100,150,@fitnessf);
trajectory_planningf_BSO(para,10,100,@fitnessf);
% [x_result,f_result,cost_time]=trajectory_planningf_PSO(para,100,100,@fitnessf);
% trajectory_planningf_GA(para,100,150,@fitnessf,@moveplot,@paraplot3);
% time_store=zeros(500,2);
% for i=100:300
% [x11,x12,x13]=trajectory_planningf_GA(para,i+10,5,@fitnessf);
% time_store(i,1)=i+10;
% time_store(i,2)=x13;
% end

% para=[px;py;theta];
% bsol=zeros(1,pointnum);
% kk=trajectory(bsol,para);
% fit=1/fitnessf(bsol,para);
% moveplot(para,bsol,fit,0);

function a=fitnessf(pop,para)
global ft_s
    [pops,~]=size(pop);
    a=zeros(1,pops);
    ft_s=zeros(3,pops);
    for i=1:pops
        a_v=[ 2;  %ft 2
              0;  %fq 1
              1]; %time 1
    %ft:力矩的惩罚项，不超过最大值时为0,这里用加速度代替
    %fq:各关节移动总幅度
    %time:经过时间
        time=pop(i,end);
        kk=trajectory(pop(i,:),para);
        poo=kk(1:3,:);
        ft=constrain(kk);
        fq=sum(sum(abs(diff(poo'))));
        fit_v=[ft,fq,time];
        fit=fit_v*a_v;
        ft_s(:,i)=[ft;fq;time];
        a(i)=1./fit;
    end
end

function zz=trajectory(sol,para)
global pointnum internum x_dimension l
    px=para(1,:);
    py=para(2,:);
    theta=para(3,:);
    time=sol(x_dimension+1);
    x_sol=linspace(0,time,x_dimension);
    xi=linspace(0,time,internum);
    x=linspace(0,time,pointnum);
    soli=interp1(x_sol,sol(1:x_dimension),x,'Spline');
    t=atan2(py,px);
    theta_s=soli.*theta;
    phi_s=t+theta_s;
    for i=1:size(phi_s,2)
        [~,conf]=invkin3(px(i),py(i),phi_s(i),l);
%         conf=[rand rand rand];
        confs(:,i)=conf';
    end
    x1i=interp1(x,confs(1,:),xi,'Spline');
    x2i=interp1(x,confs(2,:),xi,'Spline');
    x3i=interp1(x,confs(3,:),xi,'Spline');
    delta_t=sol(x_dimension+1)/internum;
    v1i=diff(x1i)/delta_t; v1i(internum)=v1i(internum-1);
    v2i=diff(x2i)/delta_t; v2i(internum)=v2i(internum-1);
    v3i=diff(x3i)/delta_t; v3i(internum)=v3i(internum-1);
    a1i=diff(v1i)/delta_t; a1i(internum)=a1i(internum-1);
    a2i=diff(v2i)/delta_t; a2i(internum)=a2i(internum-1);
    a3i=diff(v3i)/delta_t; a3i(internum)=a3i(internum-1);
    
%     xp1=csapi(xi,x1i); vp1=fnder(xp1); ap1=fnder(vp1);
%     xp2=csapi(xi,x2i); vp2=fnder(xp2); ap2=fnder(vp2);
%     xp3=csapi(xi,x3i); vp3=fnder(xp3); ap3=fnder(vp3);
%     v1i=fnval(vp1, xi); v2i=fnval(vp2, xi); v3i=fnval(vp3, xi);
%     a1i=fnval(ap1, xi); a2i=fnval(ap2, xi); a3i=fnval(ap3, xi);
    zz=[x1i;x2i;x3i;v1i;v2i;v3i;a1i;a2i;a3i];
end

function f=constrain(tt)
    vmax=[20;20;20];
    amax=[50;50;50];
    v=tt(4:6,:);
    a=tt(7:9,:);
    f=0;
    for i=1:size(tt,2)
        tv=(abs(v(1:3,i))-vmax);
        tv=tv.*(tv>=0);
        ta=(abs(a(1:3,i))-amax);
        ta=ta.*(ta>=0);
        f=f+sum(tv)+sum(ta);
    end
end

function moveplot(para,sol,fit,iter,n)
    global pointnum internum x_dimension f1 f2 f3 f4 l
    kk=trajectory(sol,para);
    poo=kk(1:3,:);
    pcart=forkin(poo,l);
    
    px=pcart(1,:);py=pcart(2,:);
    [xt,yt]=angls2links(poo,l);
    figure(f4),
    for i=1:size(xt,1)
        plot(para(1,:),para(2,:),'g*')
        hold on
        plot(px(1:i),py(1:i))
        hold on
        plot(xt(i,:)',yt(i,:)')
        axis([-2 4 -2 2])
        text(2,1.4,['t=',num2str(i/size(xt,1))])
        hold on
        text(2,1.6,['fit=',num2str(fit)])
        hold on
        text(2,1.8,['gen.',num2str(iter)])
        hold on
        plot(0,0,'bo')
        plot(xt(i,size(xt,2)/3),yt(i,size(xt,2)/3),'bo')
        plot(xt(i,size(xt,2)/3*2),yt(i,size(xt,2)/3*2),'bo')
        hold off
        pause(0)
    end

    figure(f1),
    x_sol=linspace(0,2*pi,x_dimension);
    plot(x_sol,sol(1:x_dimension),'.','color','r','MarkerSize',20);
    axis([0 2*pi -1 1])
    figure(f2),
    theta=para(3,:);
    t=atan2(para(2,:),para(1,:));
    intervern=[t-theta;t+theta];
    x=linspace(0,2*pi,pointnum);
    xi=linspace(0,2*pi,internum);
    soli=interp1(x_sol,sol(1:x_dimension),x,'Spline');
    theta_s=soli.*theta;
    phi_s=t+theta_s;
    phi_si=interp1(x,phi_s,xi,'Linear');
    plot(x,intervern,'.','color','r','MarkerSize',20);hold on;
    area(x,intervern(1,:),'FaceColor',[0 0.75 0.75]);hold on;
    area(x,intervern(2,:),'FaceColor',[0 0.75 0.75]);hold on;
    plot(xi,phi_si,'color','b');hold off;
end

function paraplot3(n,maxf,bsol)
global para
    e=[1:n];
    
    moveplot(para,bsol(n,:),1./maxf(end),size(maxf,2));
    
    figure,plot(e,1./maxf)
    xlabel('generation')
    ylabel('min. fitness')
end