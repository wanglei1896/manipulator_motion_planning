global bound link_length result npout
link_length=[1,1,0.5];
bound=[ -pi     pi;% qm1
        -pi     pi;% qm2
        -pi     pi;% qm3
       -pi/4   pi/4;% vqm1
       -pi/4   pi/4;% vqm2
       -pi/4   pi/4;% vqm3
        -pi     pi;% phis
        -pi     pi;% phif
        0       8;% t1
        0       8];%t2
npout=40;
% hh=findobj(gcf,'tag','x1');
% xs=str2num(get(hh,'string'));
% hh=findobj(gcf,'tag','y1');
% ys=str2num(get(hh,'string'));
% hh=findobj(gcf,'tag','phi');
% phis=str2num(get(hh,'string'));
% hh=findobj(gcf,'tag','x2');
% xf=str2num(get(hh,'string'));
% hh=findobj(gcf,'tag','y2');
% yf=str2num(get(hh,'string'));

% 初始点和终点的笛卡尔坐标
config_initial_c=[1,0];
config_final_c=[1,2];

% error handle
% config_initial_c and config_final_c
if norm(config_initial_c)>=norm(link_length,1)
    warndlg('there is no redundancy..... change start position','!! error !!')
    clear
    return
end
if norm(config_final_c)>=norm(link_length,1)
   warndlg('there is no redundancy..... change final position','!! error !!')
   clear
   return
end
% 组合起来，在函数间传递
config_vec_c=[config_initial_c;
              config_final_c];

sizepop = 100;
iteration_num = 100;

% cacu_result(3,para,100,120,@fitnessf);
% [x_result,f_result,cost_time]=trajectory_planningf_GA(para,100,200,@fitnessf)
% [x1,x2,x3]=trajectory_planningf_PSO(para,100,500,@fitnessf)
% fmin=BSO_fun(100, 100,-1,1,10,@fitnessf);
% [x11,x12,x13]=trajectory_planningf_BSO(para,50,100,@fitnessf);
% [x1,x2,x4]=trajectory_planningf_BAS(para,1000,@fitnessf,@trajplot3,@paraplot3);
% [x21,x22,x23,x24]=trajectory_planningf_xBSO(para,@fitnessf,@trajplot3,@paraplot3)
% time_store=zeros(1000,2);
% for i=1:1000
% npout=i+10;
% [x11,x12,x13]=trajectory_planningf_BSO(para,10,50,@fitnessf);
% time_store(i,1)=npout;
% time_store(i,2)=x13;
% end
fitnessFun(solution_history(100,:),config_vec_c)
%buildObstacle();
%[fitness_history,solution_history,cost_time] = trajectory_planningf_BSO(config_vec_c,sizepop,iteration_num,@fitnessFun);
%trajectory_evolution3(config_vec_c,solution_history,fitness_history,iteration_num);
%analytical_plot3(fitness_history, solution_history(end,:), config_vec_c);

function buildObstacle()
% 构建一个椭圆形的障碍物
global obstacle_surface
center = [1,1]';
scale = 0.4;
theta = linspace(0,2*pi,30);
x = cos(theta);
y = sin(theta);
obstacle_surface=[x;y];
obstacle_surface=obstacle_surface*scale+center;
end

function flag = isLapped(joint_trajectory)
global obstacle_surface link_length
[link_pos_x,link_pos_y]=angls2links(joint_trajectory,link_length);
in=inpolygon(link_pos_x,link_pos_y,obstacle_surface(1,:),obstacle_surface(2,:));
flag = any(any(in));
end

function group_fitness = fitnessFun(pop,config_vec_c)
global link_length npout
[pops,~]=size(pop);
group_fitness=zeros(1,pops);
for i=1:pops
    fit=0;
    weight_vec=[ 2;  %ft 2
          1.8;  %fq 1.8
          2;  %fdis 2
          1]; %time 1
    %ft:力矩的惩罚项，不超过最大值时为0,这里用加速度代替
    %fq:各关节移动总幅度
    %fdis:轨迹总长
    %time:经过时间
    each_solution=pop(i,:);
    time=each_solution(9)+each_solution(10);
    [message_initial,config_initial_q]=invkin3(config_vec_c(1,:),each_solution(7),link_length);
    [message_final,config_final_q]=invkin3(config_vec_c(2,:),each_solution(8),link_length);
%     bsol(7)=phis;
%     bsol(8)=phif;
    if strcmp(message_initial,'no-redundancy') || strcmp(message_final,'no-redundancy')
        if strcmp(message_initial,'no-redundancy')
            fit=fit+(config_initial_q(1))*1000;
        end
        if strcmp(message_final,'no-redundancy')
            fit=fit+(config_final_q(1))*1000;
        end
        group_fitness(i)=1/fit;
        continue;
    end
    config_vec_q=[config_initial_q;config_final_q];
    solution_vec=[each_solution(1:6),each_solution(9:10)];
    [joint_trajectory,~,joint_trajectory_acc]=gen_trajectory(config_vec_q,solution_vec,10);
%     tt=torque3(kk);
%     ft=ftorque3(tt);
    ft=ftorque3(joint_trajectory_acc);
    fq=sum(sum(abs(diff(joint_trajectory'))));
    pos=forkin(joint_trajectory,link_length);xx=pos(1,:);yy=pos(2,:);
    x=diff(xx);
    y=diff(yy);
    dis=sqrt(x.^2+y.^2);
    fdis=sum(dis);
    fitness_vec=[ft,fq,fdis,time];
    fit=fitness_vec*weight_vec;
    % 如果轨迹与障碍物相交，则乘上0，适应度变为0;
    % 没相交则乘上1，维持不变。
    group_fitness(i)=1/fit*~isLapped(joint_trajectory);
end
end

function f=ftorque3(tt)
% t1max=45;t2max=25;t3max=5;
t1max=10;t2max=10;t3max=10;
t1=tt(1,:);t2=tt(2,:);t3=tt(3,:);
f=0;
for i=1:size(tt,2)
    if abs(t1(i)) < t1max
        tc1=0;
    else
        tc1=abs(t1(i))-t1max;
    end
    if abs(t2(i)) < t2max
        tc2=0;
    else
        tc2=abs(t2(i))-t2max;
    end
    if abs(t3(i)) < t3max
        tc3=0;
    else
        tc3=abs(t3(i))-t3max;
    end
    f=f+tc1+tc2+tc3;
end
end

function trajectory_evolution3(config_vec_c,solution_history,fitness_history,n)
global link_length npout
%TRAJPLOT3 3杆下绘制每次迭代杆的位姿和轨迹
%   这里的返回值后面要用到，所以先返回了
for i=1:n
    [~,config_initial_q]=invkin3(config_vec_c(1,:),solution_history(i,7),link_length);
    [~,config_final_q]=invkin3(config_vec_c(2,:),solution_history(i,8),link_length);
    qs=config_initial_q;
    qm=solution_history(i,1:3);
    qg=config_final_q;
    
    [xt,yt]=angls2links([qs' qm' qg'],link_length);
    config_vec=[config_initial_q; config_final_q];
    solution_vec=[solution_history(i,1:6),solution_history(i,9:10)];
    [q,~,~]=gen_trajectory(config_vec,solution_vec,npout);
    pcart=forkin(q,link_length);
    px=pcart(1,:);py=pcart(2,:);
    plot(px,py)
    hold on
    axis([-4 4 -4 4])
    text(0,2.8,['gen. no.',num2str(i)])
    text(0,2.4,['fit=',num2str(fitness_history(i))])
    plot(xt',yt')
    hold on
    plot(0,0,'ko')
    for j=1:2
        plot(xt(:,size(xt,2)/3*j),yt(:,size(xt,2)/3*j),'bo')
    end
    hold off
    pause(0)
end
end

function analytical_plot3(fitness_history, best_solution, config_vec_c)
%PARAPLOT3 3杆情况下机械臂各参数变化图
%   n:迭代次数
%   fitness_history:最优值序列
%   best_solution:最优解
global link_length npout
    %calculate trajectory from config_vec and solution vec
    [~,config_initial_q]=invkin3(config_vec_c(1,:),best_solution(7),link_length);
    [~,config_final_q]=invkin3(config_vec_c(2,:),best_solution(8),link_length);
    config_vec=[config_initial_q; config_final_q];
    solution_vec=[best_solution(1:6),best_solution(9:10)];
    [q,qd,qdd]=gen_trajectory(config_vec,solution_vec,npout);
    
    %tractory animation
    figure,
    manimove_anime(q,link_length);
    
    %plot joint variation in best_solution
    t1=best_solution(9);ti1=linspace(0,t1,20);
    t2=best_solution(10);ti2=linspace(t1,t2+t1,20);
    time=[ti1,ti2];
    figure,
    plot(time,q(1,:),'r--',time,q(2,:),'g--+',time,q(3,:),'b-*',time(20),q(1,20),'ko',time(20),q(2,20),'ko',time(20),q(3,20),'ko')
    h = legend('joint 1','joint 2','joint 3');
    xlabel('Time(s)')
    ylabel('joint angle(rad)')
    figure,
    plot(time,qd(1,:),'r--',time,qd(2,:),'g--+',time,qd(3,:),'b-*',time(20),qd(1,20),'ko',time(20),qd(2,20),'ko',time(20),qd(3,20),'ko')
    h = legend('joint 1','joint 2','joint 3');
    xlabel('Time(s)')
    ylabel('joint vilocity(rad/s)')
    figure,
    plot(time,qdd(1,:),'r--',time,qdd(2,:),'g--+',time,qdd(3,:),'b-*',time(20),qdd(1,20),'ko',time(20),qdd(2,20),'ko',time(20),qdd(3,20),'ko')
    h = legend('joint 1','joint 2','joint 3');
    xlabel('Time(s)')
    ylabel('joint acceleration(rad/s^2)')
    
    
    %plot best_fitness variation in iteration history
    iteration=[1:size(fitness_history,2)];
    figure,
    plot(iteration,fitness_history)
    xlabel('generation')
    ylabel('min. fitness')

%     tt=torque3(kk);
% 
%     t1=bsol(8);ti1=linspace(0,t1,20);
%     ti2=bsol(9);ti2=linspace(t1,ti2+t1,20);
%     time=[ti1,ti2];
% 
% 
%     figure,plot(time,tt(1,:),'r--',time,tt(2,:),'g--+',time,tt(3,:),'b-*',time(20),tt(1,20),'ko',time(20),tt(2,20),'ko',time(20),tt(3,20),'ko')
%     h = legend('joint 1','joint 2','joint 3');
%     xlabel('Time(s)')
%     ylabel('joint tourque(N.m)')
% 
%     figure,plot(e,trec)
%     xlabel('generation')
%     ylabel('consumed time for point to point motion(s)')
%     figure,plot(e,qrec)
%     xlabel('generation')
%     ylabel('total joint distance(rad)')
%     figure,plot(e,drec)
%     xlabel('generation')
%     ylabel('total cartesian trajectory length(m)')
%     figure,plot(e,torrec)
%     xlabel('generation')
%     ylabel('total excessive torque(N.m)')
end