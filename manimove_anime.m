function manimove_anime(qt,l)
global obstacle_surface
%MANIMOVE_ANIME 机械臂的运动动画
%   此处显示详细说明
d=size(l,2);
pcart=forkin(qt,l);
px=pcart(1,:);py=pcart(2,:);
pcart2=forkin(qt(1:d-1,:),l(1:d-1));
px2=pcart2(1,:);py2=pcart2(2,:);
[xt,yt]=angls2links(qt,l);

for i=1:size(xt,1)
    plot(obstacle_surface(1,:),obstacle_surface(2,:))
    hold on
    plot(px(1:i),py(1:i))
    hold on
    plot(px2(1:i),py2(1:i))
    hold on
    plot(xt(i,:)',yt(i,:)')
    axis([-4 4 -4 4])
    text(0,2.6,['gen. no.',num2str(i)])
    hold on
    plot(0,0,'bo')
    for j=1:d-1
        plot(xt(i,size(xt,2)/d*j),yt(i,size(xt,2)/d*j),'bo')
    end
    hold off
    pause(0)
end
end

