function [q,qd,qdd]=gen_trajectory(config_vec_q,solution_vec,pointNum_out)
%generate trajctory in joint space by linear interplot
%
num_joints=size(config_vec_q,2);

% decode configuration vector
% to get initial and final point joint configuration
x0=config_vec_q(1,:);
vx0=zeros(1,num_joints);
ax0=zeros(1,num_joints);
x2=config_vec_q(2,:);
vx2=zeros(1,num_joints);
ax2=zeros(1,num_joints);

% decode the solution vector
% to get middle point joint-configuration and time-interval
x1=solution_vec(1:num_joints);
vx1=solution_vec(num_joints+1:2*num_joints);
t1=solution_vec(2*num_joints+1);
t2=solution_vec(2*num_joints+2);

% compute interpolate factor(analytical solution)
a00=x0;
a01=vx0;
a02=ax0/2;
a03=(4*x1-vx1*t1-4*x0-3*vx0*t1-ax0*t1^2)/t1^3;
a04=(vx1*t1-3*x1+3*x0+2*vx0*t1+ax0*t1^2/2)/t1^4;
ax1=2*a02+6*a03*t1+12*a04*t1^2;

b10=x1;
b11=vx1;
b12=ax1/2;
b13=(20*x2-20*x1-(8*vx2+12*vx1)*t2-(3*ax1-ax2)*t2^2)/(2*t2^3);
b14=(30*x1-30*x2+(14*vx2+16*vx1)*t2+(3*ax1-2*ax2)*t2^2)/(2*t2^4);
b15=(12*x2-12*x1-(6*vx2+6*vx1)*t2-(ax1-ax2)*t2^2)/(2*t2^5);

% 细分时间步，根据系数进行插值
t=linspace(0,t1,pointNum_out/2);
x01=a00'+a01'*t+a02'*t.^2+a03'*t.^3+a04'*t.^4;
vx01=a01'+2*a02'*t+3*a03'*t.^2+4*a04'*t.^3;
ax01=2*a02'+6*a03'*t+12*a04'*t.^2;

t=linspace(0,t2,pointNum_out/2);
x12=b10'+b11'*t+b12'*t.^2+b13'*t.^3+b14'*t.^4+b15'*t.^5;
vx12=b11'+2*b12'*t+3*b13'*t.^2+4*b14'*t.^3+5*b15'*t.^4;
ax12=2*b12'+6*b13'*t+12*b14'*t.^2+20*b15'*t.^3;

% merge two interval
q=[x01 x12];
qd=[vx01 vx12];
qdd=[ax01 ax12];



