function cacu_result(k,para,pops,n,fitnessf)
global result
f_result_GA=0;rec_result_GA=[0;0;0;0];cost_time_GA=0;
f_result_PSO=0;rec_result_PSO=[0;0;0;0];cost_time_PSO=0;
f_result_BSO=0;rec_result_BSO=[0;0;0;0];cost_time_BSO=0;
% f_result_xBSO=0;rec_result_xBSO=[0;0;0;0];cost_time_xBSO=0;
for i=1:10
    i
    [x_result,f_result,cost_time]=trajectory_planningf_GA(para,pops,n,fitnessf);
    f_result
    f_result_GA(i)=f_result;
%     rec_result_GA=rec_result_GA+rec_result;
    cost_time_GA(i)=cost_time;
    [x_result,f_result,cost_time]=trajectory_planningf_PSO(para,pops,n,fitnessf);
    f_result
    f_result_PSO(i)=f_result;
%     rec_result_PSO=rec_result_PSO+rec_result;
    cost_time_PSO(i)=cost_time;
    [x_result,f_result,cost_time]=trajectory_planningf_BSO(para,pops,n,fitnessf);
    f_result
    f_result_BSO(i)=f_result;
%     rec_result_BSO=rec_result_BSO+rec_result;
    cost_time_BSO(i)=cost_time;
%     [x_result,f_result,rec_result,cost_time]=trajectory_planningf_xBSO(para,recor);
%     f_result_xBSO=f_result_xBSO+f_result;
%     rec_result_xBSO=rec_result_xBSO+rec_result;
%     cost_time_xBSO=cost_time_xBSO+cost_time;
end
result(:,3*(k-1)-2:3*(k-1))=[mean(f_result_GA),mean(f_result_PSO),mean(f_result_BSO);
                            sqrt(var(f_result_GA)),sqrt(var(f_result_PSO)),sqrt(var(f_result_BSO));];
end

