function f = forkin(t,l)
% a1=1;a2=1;a3=0.5;
d=size(t,1);
kt=t;
kt(1,:)=t(1,:);
if d>=2
    for i=2:d
        kt(i,:)=kt(i-1,:)+t(i,:);
    end
end
x=l*cos(kt);
y=l*sin(kt);
phi=kt(d,:);
f=[x; y ;phi];