global result
% trajectory_planning2f
% trajectory_planning3f
% trajectory_planning4f
% trajectory_planning5f
result_f=[result(1,1:3);result(1,4:6);result(1,7:9);result(1,10:12)]';
% result_t=[result(2,1:3);result(2,4:6);result(2,7:9);result(2,10:12)]';
% result_ft=[result(3,1:3);result(3,4:6);result(3,7:9);result(3,10:12)]';
% result_fq=[result(4,1:3);result(4,4:6);result(4,7:9);result(4,10:12)]';
% result_fdis=[result(5,1:3);result(5,4:6);result(5,7:9);result(5,10:12)]';
% result_time=[result(6,1:3);result(6,4:6);result(6,7:9);result(6,10:12)]';
x=[2,3,4,5];

b=bar(result_f');
grid on;
ch = get(b,'children');
legend('GA','PSO','BSO');
xlabel('杆数')
ylabel('关节转角幅度(theta)')
axis([0,6,0,6])  %确定x轴与y轴框图大小
set(gca,'XTickLabel',(2:1:6))

% figure, 
% b=bar(result_t');
% grid on;
% ch = get(b,'children');
% legend('GA','PSO','BSO');
% xlabel('杆数')
% ylabel('平均运行时间(s)')
% axis([0,6,0,10])
% set(gca,'XTickLabel',(2:1:6))

% figure, 
% b=bar(result_ft');
% grid on;
% ch = get(b,'children');
% legend('GA','PSO','BSO');
% xlabel('杆数')
% ylabel('平均超出力矩')
% axis([0,6,0,10])
% set(gca,'XTickLabel',(2:1:6))
% 
% figure, 
% b=bar(result_fq');
% grid on;
% ch = get(b,'children');
% legend('GA','PSO','BSO');
% xlabel('杆数')
% ylabel('平均关节移动总距离')
% axis([0,6,0,10])
% set(gca,'XTickLabel',(2:1:6))
% 
% figure, 
% b=bar(result_fdis');
% grid on;
% ch = get(b,'children');
% legend('GA','PSO','BSO');
% xlabel('杆数')
% ylabel('平均路径长度(m)')
% axis([0,6,0.5,4])
% set(gca,'XTickLabel',(2:1:6))
% 
% figure, 
% b=bar(result_time');
% grid on;
% ch = get(b,'children');
% legend('GA','PSO','BSO');
% xlabel('杆数')
% ylabel('平均运动时间(s)')
% axis([0,6,0,5])
% set(gca,'XTickLabel',(2:1:6))