global l1 l2 l3
l1=1; l2=1; l3=0.5;
t1=1;

a0=0;
a1=1;
a2=0;
a3=0;
a4=0;
% w=[1,-1,10];

spacenum=200;
t=linspace(0,t1,spacenum);
%s是曲线参数，下面给出该参数与时间的函数关系，也叫运动法则
s=a0+a1*t+a2*t.^2+a3*t.^3+a4*t.^4;
s=s*2*pi;
% x01=w'*s;
x01=[   s;
        -3*s;
        s];
manimove_anime(x01,[l1 l2 l3]);
% pcart=forkin(x01,[l1 l2 l3]);
% px=pcart(1,:);py=pcart(2,:);
% pcart2=forkin(x01(1:2,:),[l1 l2]);
% px2=pcart2(1,:);py2=pcart2(2,:);
% [xt,yt]=angls2links(x01,[l1 l2 l3]);
% 
% for i=1:size(xt,1)
%     plot(px(1:i),py(1:i))
%     hold on
%     plot(px2(1:i),py2(1:i))
%     hold on
%     plot(xt(i,:)',yt(i,:)')
%     axis([-4 4 -4 4])
%     text(0,2.6,['gen. no.',num2str(i)])
%     hold on
%     plot(0,0,'bo')
%     plot(xt(i,size(xt,2)/3),yt(i,size(xt,2)/3),'bo')
%     plot(xt(i,size(xt,2)/3*2),yt(i,size(xt,2)/3*2),'bo')
%     hold off
%     pause(0)
% end