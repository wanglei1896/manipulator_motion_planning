function [x_result,f_result,cost_time,it] = trajectory_planningf_GA(para,pops,maxgen,fitnessf,interplot,sumplot)
%GA需解耦部分：
%解向量编码
%适应函数 fitnessf
%绘图展示数据部分

global bound rng
if nargin==5
    sumplot=interplot;
end
%GA parameters
% pops=50;
crossprop=0.90;
mutprop=0.05;
% maxgen=200;
%initialization
numvar=size(bound,1);
rng=(bound(:,2)-bound(:,1))';
pop=zeros(pops,numvar);
% pop = ones(pops,1)*[-0.929115748796937,1.51672911292210,1.99474036827404,0.785398163397448,-0.0904302695568150,-0.143345191582938,2.57246342182134,1.87165628967900,0.0727427807745007,0.602902333253355];
pop(:,1:numvar)=(ones(pops,1)*rng).*(rand(pops,numvar))+...
    (ones(pops,1)*bound(:,1)');
% bsol_store=zeros(maxgen,numvar);
tic
it=1;

while it<5 || (norm(bsol_store(it-1,:)-bsol_store(it-2,:))+norm(bsol_store(it-2,:)-bsol_store(it-3,:))...
        +norm(bsol_store(it-3,:)-bsol_store(it-4,:)))/(3*norm(bsol_store(it-1,:)))>0.0001
    fpop=fitnessf(pop, para);
    [bfit,inds]=max(fpop);
    maxf(it)=bfit;
    bsol=pop(inds,:);    % best solution. 
    bsol_store(it,:)=bsol;
    % tournament selection
    toursize=5;
    players=ceil(pops*rand(pops,toursize));
    scores=fpop(players);
    [a,m]=max(scores');
    pind=zeros(1,pops);
    for ii=1:pops
        pind(ii)=players(ii,m(ii));
        parent(ii,:)=pop(pind(ii),:);
    end
    %arithmatic crossover
    offs=cross_singlepoint(parent,crossprop);
%     offs=cross1(parent,crossprop);
    %mutate1=uniform mutation.
    moffs=mutate1(offs,mutprop);
    pop=moffs;
    %*************************plot results
    if nargin==6
        feval(interplot,para,bsol,1/bfit,it,maxgen);
        pause(0)
    end
    it=it+1;
end
toc
if nargin>4
    sumplot(it-1,maxf,bsol_store);
end

x_result = bsol;
f_result = 1/maxf(it-1);
cost_time = toc;
it=it-1;
end