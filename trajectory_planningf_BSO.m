function [fitness_history,solution_history,cost_time] = trajectory_planningf_BSO(config_vec_c,sizepop,iteration_num,fitnessf)
%TRAJECTORY_PLANNING3F_BSO 此处显示有关此函数的摘要
%   config_vec_c:控制参数(笛卡尔坐标下起始点与终点位置)
%   sizepop:种群数量
%   iteration_num:迭代次数
%   fitnessf:适应度函数
global bound
%% 参数初始化
k=size(bound, 1);   %space dimension

fitness_history=zeros(1,iteration_num);
solution_history=zeros(iteration_num,k);

%antenna distance
d0=0.001;
d1=3;
d=d1/2;
eta_d=0.98;
%steps
step = rands(sizepop, k);
step_l=1;%step length
eta_step=0.98;
%学习因子
c1=1.49445;
c2=1.49445;
%比例因子，决定PSO中的速度与BAS中步长的混合比例
lamda=0.2;

Vmax = 1;
Vmin = -1;

%% 生成初始天牛群和速度
pop = (ones(sizepop,1)*(bound(:,2)-bound(:,1))').*(rand(sizepop,k))+...
    (ones(sizepop,1)*bound(:,1)');    %初始种群
V = rands(sizepop,k);  %初始化速度
% 计算适应度
% for i=1:k
    fitness = fitnessf(pop, config_vec_c);
% end

%% 个体极值和群体极值
[bestfitness, bestindex] = max(fitness);
zbest = pop(bestindex,:);   %全局最佳
gbest = pop;    %个体最佳
fitnessgbest = fitness;   %个体最佳适应度值
fitnesszbest = bestfitness;   %全局最佳适应度值

%% 迭代寻优
tic
% figure,
for i=1:iteration_num
    for j = 1:sizepop
        % 速度更新
        V(j,:) = V(j,:) + c1*rand*(gbest(j,:) - pop(j,:)) + c2*rand*(zbest - pop(j,:));
        V(j,find(V(j,:)>Vmax)) = Vmax;
        V(j,find(V(j,:)<Vmin)) = Vmin;
        % 步长更新
        dir = V(j,:)/(eps+norm(V(j,:)));
        xleft=pop(j,:)+dir*d;
        fleft=fitnessf(xleft, config_vec_c);
        xright=pop(j,:)-dir*d;
        fright=fitnessf(xright, config_vec_c);
        step(j,:)=step_l*dir*sign(fleft-fright);
        % 种群更新
        pop(j,:) = pop(j,:) + lamda*V(j,:) + (1-lamda)*step(j,:);
        max_i=find(pop(j,:)>bound(:,2)');
        pop(j,max_i) = bound(max_i,2)';
        min_i=find(pop(j,:)<bound(:,1)');
        pop(j,min_i) = bound(min_i,1)';
    end
    
    % 适应度值更新
    fitness = fitnessf(pop, config_vec_c);
    for j = 1:sizepop
        % 个体最优更新
        if fitness(j) > fitnessgbest(j)
            gbest(j,:) = pop(j,:);
            fitnessgbest(j) = fitness(j);
        end
        % 群体最优更新
        if fitness(j) > fitnesszbest
            zbest = pop(j,:);
            fitnesszbest = fitness(j);
        end
    end
    % 存储当前迭代最优结果
    [best_fit,best_index]=max(fitness);
    fitness_history(i)=1/best_fit;
    bsol=pop(best_index,:);
    solution_history(i,:)=bsol;
    %步长衰减
    d=d*eta_d+d0;
    step_l=step_l*eta_step;
end
toc

cost_time=toc;
end