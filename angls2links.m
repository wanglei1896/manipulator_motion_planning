function [xx,yy]=angls2links(bq,l)
% bq中每一列为机械臂中一个关节的坐标(theta1,theta2,theta3)
d=size(bq,1);
x=zeros(d,1);
y=zeros(d,1);
for i=1:size(bq,2)
    for j=1:d
        f=forkin(bq(1:j,i),l(1:j));
        x(j)=f(1,:);
        y(j)=f(2,:);
    end
    xxt=linspace(0,x(1));
    yyt=linspace(0,y(1));
    for j=2:d
        xxi=linspace(x(j-1),x(j));
        xxt=[xxt xxi];
        yyi=linspace(y(j-1),y(j));
        yyt=[yyt yyi];
    end
    xx(i,:)=xxt;
    yy(i,:)=yyt;
end

        
     
      