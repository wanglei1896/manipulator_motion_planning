function [b,a]=invkin5(x,y,phi1,phi2,phi3,l)
x1=x-l(5)*cos(phi1);
y1=y-l(5)*sin(phi1);
x2=x1-l(4)*cos(phi2);
y2=y1-l(4)*sin(phi2);
x3=x2-l(3)*cos(phi3);
y3=y2-l(3)*sin(phi3);
k=(x3^2+y3^2-l(1)^2-l(2)^2)/(2*l(1)*l(2));
t2=acos(k);
while k > 1
    b='no-redundancy';
    a=[k 0 0 0 0];
    return;
end
d=l(1)^2+l(2)^2+2*l(1)*l(2)*cos(t2);
cc=(x3*(l(1)+l(2)*cos(t2))+y3*l(2)*sin(t2))/d;
ss=(-x3*l(2)*sin(t2)+y3*(l(1)+l(2)*cos(t2)))/d;
t1=atan2(ss,cc);
t3=phi3-t1-t2;
t4=phi2-phi3;
t5=phi1-phi2;
b='ok';
a=[t1 t2 t3 t4 t5];
end

