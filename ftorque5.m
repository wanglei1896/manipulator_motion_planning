function f=ftorque5(tt)
% t1max=45;t2max=20;t3max=10;t4max=5;
t1max=20;t2max=20;t3max=20;t4max=20;t5max=20;
t1=tt(1,:);t2=tt(2,:);t3=tt(3,:);t4=tt(4,:);t5=tt(5,:);
f=0;
for i=1:size(tt,2)
    if abs(t1(i)) < t1max
        tc1=0;
    else
        tc1=abs(t1(i))-t1max;
    end
    if abs(t2(i)) < t2max
        tc2=0;
    else
        tc2=abs(t2(i))-t2max;
    end
    if abs(t3(i)) < t3max
        tc3=0;
    else
        tc3=abs(t3(i))-t3max;
    end
    if abs(t4(i)) < t4max
        tc4=0;
    else
        tc4=abs(t4(i))-t4max;
    end
    if abs(t5(i)) < t5max
        tc5=0;
    else
        tc5=abs(t5(i))-t5max;
    end
    f=f+tc1+tc2+tc3+tc4+tc5;
end

