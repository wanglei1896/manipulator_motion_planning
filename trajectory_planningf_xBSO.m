function [x_result,f_result,rec_result,cost_time] = trajectory_planningf_xBSO(para,recor,trajplot,paraplot)
%TRAJECTORY_PLANNING3F_BSO 此处显示有关此函数的摘要
%   此处显示详细说明
global bound
%% 参数初始化
n=80;  %iterations
k=size(bound, 1);   %space dimension
sizepop=80;    %populations

fbest_store=zeros(1,n);
rec=zeros(4,n);

%antenna distance
d0=0.001;
d1=1;
d=d1/2;
eta_d=0.95;
%steps
step = rands(sizepop, k);
c=0.5;    %须长与步长之比
step_l=d/c;    %步长
%学习因子
c1=1.49445;
c2=1.49445;
%比例因子，决定PSO中的速度与BAS中步长的混合比例
lamda=0.2;

%% 生成初始天牛群和速度
% 各维度待优化参数归一化
pop = rand(sizepop,k);    %初始种群
V = rands(sizepop,k);  %初始化速度
% 速度归一化
for j = 1:sizepop
    V(j,:) = V(j,:)/(norm(V(j,:)));
end
% 计算适应度时把归一化的参数值还原
pop_r = (ones(sizepop,1)*(bound(:,2)-bound(:,1))').*pop+ones(sizepop,1)*bound(:,1)';
fitness = fitnesstraf(pop_r, para, recor);

%% 个体极值和群体极值
[bestfitness bestindex] = max(fitness);
zbest = pop(bestindex,:);   %全局最佳
gbest = pop;    %个体最佳
fitnessgbest = fitness;   %个体最佳适应度值
fitnesszbest = bestfitness;   %全局最佳适应度值

%% 迭代寻优
tic
% figure,
for i=1:n
    for j = 1:sizepop
        % 速度更新
        V(j,:) = V(j,:) + c1*rand*(gbest(j,:) - pop(j,:)) + c2*rand*(zbest - pop(j,:));
        V(j,:) = V(j,:)/(eps+norm(V(j,:)))*step_l;
%         V(j,find(V(j,:)>Vmax)) = Vmax;
%         V(j,find(V(j,:)<Vmin)) = Vmin;
        % 步长更新
        dir = V(j,:)/step_l;
%         dir = rands(k,1)';
%         dir = dir/(eps+norm(dir));
        xleft=pop(j,:)+dir*d;
        fleft=fitnesstraf(xleft.*(bound(:,2)-bound(:,1))'+bound(:,1)', para, recor);
        xright=pop(j,:)-dir*d;
        fright=fitnesstraf(xright.*(bound(:,2)-bound(:,1))'+bound(:,1)', para, recor);
        step(j,:)=step_l*dir*sign(fleft-fright);
        % 种群更新
        pop(j,:) = pop(j,:) + lamda*V(j,:) + (1-lamda)*step(j,:);
        pop(j,k-2+find(pop(j,[k-1,k])>1)) = 1;
        pop(j,k-2+find(pop(j,[k-1,k])<0)) = 0;
    end
    % 适应度值更新
    pop_r = (ones(sizepop,1)*(bound(:,2)-bound(:,1))').*pop+ones(sizepop,1)*bound(:,1)';
    fitness = fitnesstraf(pop_r, para, recor);
    for j = 1:sizepop
        % 个体最优更新
        if fitness(j) > fitnessgbest(j)
            gbest(j,:) = pop(j,:);
            fitnessgbest(j) = fitness(j);
        end
        % 群体最优更新
        if fitness(j) > fitnesszbest
            zbest = pop(j,:);
            fitnesszbest = fitness(j);
        end
    end
    fbest_store(i)=fitnesszbest;
    rec(:,i)=feval(recor,zbest.*(bound(:,2)-bound(:,1))'+bound(:,1)',para);
    %步长衰减
    d=d*eta_d+d0;
    step_l=d/c;
    %*************************plot results
%     kk=feval(trajplot,para,zbest.*(bound(:,2)-bound(:,1))'+bound(:,1)',i,n);
%     pause(0)
end
toc
% feval(paraplot,n,fbest_store,kk,zbest.*(bound(:,2)-bound(:,1))'+bound(:,1)');

x_result=zbest.*(bound(:,2)-bound(:,1))'+bound(:,1)';
f_result=1/fbest_store(n);
rec_result=rec(:,n);
cost_time=toc;
end