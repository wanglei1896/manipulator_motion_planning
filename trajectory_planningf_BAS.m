function [x_result,f_result,cost_time]=trajectory_planningf_BAS(para,n,fitnessf,interplot,sumplot)
global bound
if nargin==4
    sumplot=interplot;
end
%antenna distance
d0=0.001;
d1=2;
d=d1;
eta_d=0.999;
%steps
c=2;
step=d/c;%step length

% n=5000;%iterations
k=size(bound, 1);%space dimension
rng=(bound(:,2)-bound(:,1));
x0=rng.*rand(k,1)+bound(:,1);
x=x0;
xbest=x0;
fbest=fitnessf(xbest', para);
%fbest_store=fbest;
%x_store=[0;x;fbest];
fbest_store=zeros(1,n);

tic
% figure,
for i=1:n
    dir=rands(k,1);
    dir=dir/(eps+norm(dir));
    xleft=x+dir*d;
    fleft=fitnessf(xleft', para);
    xright=x-dir*d;
    fright=fitnessf(xright', para);
    x=x+step*dir*sign(fleft-fright);
    if(x(k-1)<0)
        x(k-1)=0;
    end
    if(x(k)<0)
        x(k)=0;
    end
    f=fitnessf(x', para);
    %%%%%%%%%%%
    if f>fbest
        xbest=x;
        fbest=f;
    end
    %%%%%%%%%%%
    f_store(i)=f;
    x_diff(i)=norm(x-x0);
    fbest_store(i)=fbest;
    bsol_store(i,:)=x';
    %%%%%%%%%%%
    d=d*eta_d+d0;
    step=d/c;
    %*************************plot results
    if nargin==5
        interplot(para,x',1/fbest_store(i),i,n);
        pause(0)
    end
end
toc
if nargin>3
    sumplot(n,f_store,bsol_store);
end
% paraplot(n,1./x_diff,kk,x');

x_result=x';
f_result=1/fbest_store(n);
cost_time=toc;
end