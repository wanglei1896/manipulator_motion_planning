﻿# 机械臂运动规划

#### 项目介绍
修改了Ali Talib Oudah的Optimization of Robot Motion Planning Using Genetic Algorithm代码，改用BAS算法优化。

#### 软件架构
- **aliwork.m**:主程序，主要实现了aliwork.fig的GUI中相关控件的回调函数，程序由此启动。
- **trajectory_planning2f_GA.m**:代码中原始的遗传算法，两杆无障碍物的情况，封装成了函数。
- **trajectory_planning2f_BAS.m**:自己写的天牛须优化算法（待改进）。
- **trajectory_planning2f_PSO.m**:复现的PSO算法。
- **trajectory_planning2f_BSO.m**:复现的BSO算法。
- **trajectory_planning2f.m**:测试、对比各算法时用的脚本。
- **fitnesstra2f.m**:适应度函数，两杆无障碍物的情况。
- **forkin.m**:两杆情况下的正运动学解，根据各关节变量解出末端执行器的直角坐标。
- **invkin.m**:两杆情况下的逆运动学解，根据末端执行器的直角坐标解出各关节变量。
- **trajt.m**:两杆情况下，根据给出的起始点与终点坐标以及一组待优化参数，计算出并返回这组参数下的运动轨迹。运动轨迹以矩阵存储，每列表示一定时间步长分割下的点的运动信息，包括位置、速度、加速度。
- **torque.m**:两杆情况下，根据之前求出的运动轨迹，计算出轨迹上经过每个点时两个关节的力矩。
- **ftorque.m**:两杆情况下，两个关节力矩的惩罚函数。


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. 从*aliwork.m*启动，出现GUI界面
2. 选择两杆无障碍物的情况，输入起始点和终点坐标，点击开始后执行的是*trajectory_planning2f.m*中的脚本

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
